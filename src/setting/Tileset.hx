package setting;

import h2d.Tile;

class Tileset {

    public static var tile:Tile;
    public static var h:Int;
    public static var w:Int;
    public static var th:Int;
    public static var tw:Int;

    public static function init() {
        tile = hxd.Res.load("tilesetV2.png").toTile();
        h=128;
        w=128;
        th=64;
        tw=128;
    }

}