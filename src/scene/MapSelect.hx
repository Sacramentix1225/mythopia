package scene;


import h2d.Tile;
import h2d.Slider;
import hxd.res.DefaultFont;
import h2d.Bitmap;
import hxd.Res;
import h2d.Scene;
import h2d.Flow;
import h2d.Text;
import scene.gameUI.TextBar;
import hxd.Window;
import hxd.res.TiledMap;


/**
 * Class to create a screen display of the map selection menu.
 * Ysou can insert all UI element that you want to display.
 * You can find preBuild UI element in scene.gameUI
 */

class MapSelect extends DynamicScene  {
    private var scene:Scene;
    private var background:Bitmap;
    private var slider:Slider;
    private var flowScene : h2d.Flow;
    private var flowSceneBack : Bitmap;     
    
    /**
     * Create a new scene of the map selection menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    public function new() {
        super();

        
        /**
         * get the size of the current window
         */

        this.width = Window.getInstance().width;
        this.height = Window.getInstance().height;

        /**
         * background image of the menu
         */

        var bg = Res.MainMenu.Menu4k.toTile();
        bg.scaleToSize(this.width, this.height);
        background = new Bitmap(bg);
        background.smooth = true;

        /**
         * flow Element that contain a vertical aligned button for each map in the folder res/map 
         */

        flowScene = new h2d.Flow(this);
		flowScene.layout = Vertical;
		flowScene.verticalSpacing = 1;
		flowScene.padding = 2;
        flowScene.minWidth = Std.int(0.5 * width);
        flowScene.backgroundTile = Tile.fromColor(0x5d5d5d);
        flowScene.backgroundTile.scaleToSize(flowScene.outerWidth, flowScene.outerHeight);
        
        /**
         * load all map in the folder res/map and create a scene.gameUI.TextBar element (button) that add to flowScene
         */

        var mapList = hxd.Res.load("map");
        for (map in mapList) {
            var trueMap:TiledMap = map.to(TiledMap);
            var bar = new TextBar(DefaultFont.get(), trueMap.name);

            /**
             * change the current scene displayed for a scene of the map you click 
             */

            bar.interactive.onClick = function( e : hxd.Event ) {
                Game.currentScene = new AdvMap(trueMap.toMap());
                Game.instance.setScene(Game.currentScene);
                  
            }
            flowScene.addChild(bar);
    
        }
        
        /**
         * vertical slider stick to the left of FlowScene that allow the user to scroll down to see all map
         */

        slider = new Slider(this.height, 10, this);
        slider.rotate(1.5708);
        slider.x = 0.5 * width + 10;
        slider.onChange = function() {
            flowScene.y = - slider.value * (flowScene.outerHeight - height * 0.5);

            
        }



        var ftile = Tile.fromColor(0x5d5d5d);
        ftile.scaleToSize(width*0.5, height);
        flowSceneBack = new Bitmap(ftile);
        this.addChild(background);
        //this.addChild(flowSceneBack);
        this.addChild(flowScene);
        this.addChild(slider);
        
         
    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */

    override public function onResize() {
        width = Window.getInstance().width;
        height = Window.getInstance().height;
        background.tile.scaleToSize(width, height);
        flowSceneBack.tile.scaleToSize(width*0.5, height);
        slider.x = 0.5 * width + 10;
        flowScene.removeChildren();
        var mapList = hxd.Res.load("map");
        for (map in mapList) {
            var trueMap:TiledMap = map.to(TiledMap);
            var bar = new TextBar(DefaultFont.get(), trueMap.name);
            bar.interactive.onClick = function( e : hxd.Event ) {
                Game.currentScene = new AdvMap(trueMap.toMap());
                Game.instance.setScene(Game.currentScene);
                  
            }
            flowScene.addChild(bar);
    
        }
        
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        
        
    }
    
    
     
}