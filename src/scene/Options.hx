package scene;


import h2d.Slider;
import h2d.Bitmap;
import hxd.Res;
import h2d.Scene;
import h2d.Text;
import scene.gameUI.TextButton;
import hxd.Window;
import h2d.Font;

/**
 * Class to create a screen display of the options menu
 * you can insert all UI element that you want to display. 
 * You can find preBuild UI element in scene.gameUI
 */

class Options extends DynamicScene {
    private var scene:Scene;
    
    private var background:Bitmap;
    private var masterVolumeSlider:Slider;
    private var backButton:TextButton;
    
    /**
     * Create a new scene of the options menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    public function new() {
        super();

        /**
         * get the size of the current window
         */

        this.width = Window.getInstance().width;
        this.height = Window.getInstance().height;

        /**
         * create slider to change the volume
         */

        masterVolumeSlider = new Slider(250, 40, this);
        masterVolumeSlider.x = (width-250)*0.5;
        masterVolumeSlider.y = height*0.4;

        /**
         * create a button to return to the menu
         */

        backButton = new TextButton(90,90, FontFolder.smythe80, "<");
        backButton.x = 3;
        backButton.y = 3;
        backButton.label.y -= 10;

        /**
         * function executed when you click on the button
         */

        backButton.interactive.onClick = function( e : hxd.Event ) {
            Game.currentScene = new Menu();
            Game.instance.setScene(Game.currentScene);
        }

        this.addChild(backButton);
        
    }

    
    /**
     * code below is executed each time the window is resized ( don't work on js )
     */

    override public function onResize() {
        
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        
        
    }
    
     
}