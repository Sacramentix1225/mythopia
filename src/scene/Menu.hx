package scene;



import hxd.res.TiledMap;
import h2d.Bitmap;
import hxd.Res;
import h2d.Scene;
import h2d.Text;
import scene.gameUI.TextButton;
import hxd.Window;
import h2d.Console;

    /**
    * Class to create a screen display of the main menu ( displayed when you start the game ).
    * You can insert all UI element that you want to display. 
    * You can find preBuild UI element in scene.gameUI
    */

class Menu extends DynamicScene  {
    private var scene:Scene;
    private var background:Bitmap;
    private var title:Text;
    private var playButton:TextButton;
    private var optionsButton:TextButton;

    /**
     * Create a new scene of the options menu
     * if you want to display it don't forget to change the current scene of the game with Game.instance.setScene( insert the new scene )
     */

    public function new() {
        super();
        

        this.width = Window.getInstance().width;
        this.height = Window.getInstance().height;

        var bg = Res.MainMenu.Menu4k.toTile();
        bg.scaleToSize(this.width, this.height);
        background = new Bitmap(bg);
        background.smooth = true;

        title = new h2d.Text(FontFolder.smythe250);
        title.textColor = 0xD4AF37;
        title.text ="MYTHOPIA";
        title.maxWidth = width;
        title.textAlign = Center;
        title.x = 0;
        title.y = 15; // Change this if you change the font for good alignement
        title.smooth = true;

        playButton = new TextButton(350,100, FontFolder.smythe80, "Play");
        playButton.x = (this.width-350)*0.5;
        playButton.y = (this.height-100)*0.6;
        playButton.interactive.onClick = function( e : hxd.Event ) {
            Game.currentScene = new MapSelect();
            Game.instance.setScene(Game.currentScene);
        }

        optionsButton = new TextButton(350,100, FontFolder.smythe80, "Options");
        optionsButton.x = (this.width-350)*0.5;
        optionsButton.y = playButton.y + 100 + 15;
        optionsButton.interactive.onClick = function( e : hxd.Event ) {
            Game.currentScene = new scene.Options();
            Game.instance.setScene(Game.currentScene);
        }
        
        
        this.addChild(background);
        this.addChild(title);
        this.addChild(playButton);
        this.addChild(optionsButton);
        var console = new Console(hxd.res.DefaultFont.get());
        this.addChild(console);
         
    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */


    override public function onResize() {
        width = Window.getInstance().width;
        height = Window.getInstance().height;
        background.tile.scaleToSize(width, height);
        playButton.x = (this.width-350)*0.5;
        playButton.y = (this.height-100)*0.6;
        optionsButton.x = (this.width-350)*0.5;
        optionsButton.y = playButton.y + 100 + 15;
        title.maxWidth = width;
        
        
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        
        
    }
    
    
     
}