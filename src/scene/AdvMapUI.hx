package scene;


import pattern.Observer.Watcher;
import h2d.Interactive;
import h3d.pass.Default;
import hxd.res.DefaultFont;
import map.object.Ressource;
import hxd.Res;
import h2d.Bitmap;
import h2d.Object;
import h2d.Console;
import h2d.Scene;
import hxd.Window;
import h2d.Text;
import h2d.Font;
import h2d.Tile;
import h2d.Layers;
import hxd.Key in K;
import scene.gameUI.TextButton;
import h2d.col.Point;
import map.Turn;
import map.Camera;
import setting.Tileset;
import scene.gameUI.InfoBar;
import scene.gameUI.ScrollBox;


using pattern.Observer.Observables;

 
class AdvMapUI 
{
    private static var parent:Scene;
    public static var backButton:TextButton;
    public static var ressourceBar:Object;
    public static var ressourceLabels:Array<Text>;
    public static var moveInfo:Text;
    public static var nextTurnButton:TextButton;
    public static var font:Font;// Text default font
    public static var fps:h2d.Text; // label that contains fps counter
    public static var fpsint:Float; // current fps
    /**Text UI that contain the info of selected tile ( debug purpose only ) **/
    public static var tileInfo:Text;
    public static var command:Text;
    public static var cmd:Tile;
    public static var console:Console;
    public static var heroDropDown:InfoBar;
    public static var scrollBox:ScrollBox;
    
    public static var currentMousePosition:h2d.col.Point;
    public static var lastMousePosition:h2d.col.Point;

    public static var center:Point;
    public static var key : { left : Bool, right : Bool, up : Bool, down : Bool}; // insert all controll here
    public static var event : hxd.WaitEvent; 
    public static var speed = 1.; // for real time animation
    
    public static var zoomLevel:Array<Float> = [
		0.021, 0.031, 0.042, 0.062, 0.083, 0.125, 0.167, 0.2, 0.25, 0.33, 0.5, 1, 2, 3, 4, 5, 6]; // all level of zoomLevel 

    public static var zoomLevelPos:Int = 2; // default zoomLevel position on the array zoomLevel ( by defaut position 11 = x1 zoomLevel )
    public static var disp : h2d.Tile; // for moving water with displacement filter (WIP)


    public static function init(scene:Scene) {

        AdvMap.layers.setScale(zoomLevel[zoomLevelPos]);

        parent = scene;
        lastMousePosition = new Point();
        currentMousePosition = new Point();
        center = new Point( Window.getInstance().width*0.5,  Window.getInstance().height*0.5 );

        //disp = hxd.Res.normalmap.toTile();
        event = new hxd.WaitEvent();
        //pad = hxd.Pad.createDummy();
        //hxd.Pad.wait(function(p) this.pad = p);

        font = hxd.res.DefaultFont.get();
        tileInfo = new h2d.Text(font);
        tileInfo.textColor = 0xfefefe;
        tileInfo.x = 250;
        tileInfo.y = 35;
        tileInfo.text = "Tile selected here.";
        parent.addChild(tileInfo);

        command = new h2d.Text(font);
        command.textColor = 0xfefefe;
        command.x = 250;
        command.y = 5;
        command.text = "zqsd/wasd or arrow to move the camera, ctrl + mouse wheel to zoomLevel, click on the hero then click on your destination";
        parent.addChild(command);

        fps = new h2d.Text(font);
        fps.textColor = 0xfefefe;
        fps.x = 250;
        fps.y = 55;
        fpsint = hxd.Timer.fps();
        fps.text = Std.string(Math.round(fpsint));
        parent.addChild(fps);

        backButton = new TextButton(70,70, FontFolder.smythe80, "<");
        backButton.x = 5;
        backButton.y = 5;
        backButton.label.y -= 10;
        backButton.interactive.onClick = function( e : hxd.Event ) {
            parent = new Menu();
            Game.instance.setScene(parent);
        }

        /*var scrollInteract = new Interactive(270, 189, parent);
        scrollInteract.onWheel = function( e : hxd.Event ) {
            scrollBox.scrollBy(0, e.wheelDelta);

        }
        scrollInteract.y = Window.getInstance().height - 190;*/

        nextTurnButton = new TextButton(70,70, FontFolder.smythe80, "*");
        nextTurnButton.x = Window.getInstance().width - 75;
        nextTurnButton.y = Window.getInstance().height - 75;
        nextTurnButton.label.y -= 10;
        nextTurnButton.interactive.onClick = function( e : hxd.Event ) {
            Turn.next();
        }
        {
        ressourceBar = new Object(parent);
        ressourceLabels = new Array<Text>();
        ressourceBar.x = Window.getInstance().width * 0.1 - 50;
        ressourceBar.y = 3;
        var leftPart = new Bitmap(hxd.Res.MapUI.resBar.toTile(), ressourceBar);
        var coreTile = hxd.Res.MapUI.resBarCore.toTile();
        coreTile.scaleToSize(Window.getInstance().width*0.8, 50);
        var core = new Bitmap(coreTile, ressourceBar);
        core.x = 50;
        var rightPartTile = hxd.Res.MapUI.resBar.toTile();
        rightPartTile.flipX();
        var rightPart = new Bitmap(rightPartTile, ressourceBar);
        rightPart.x = Window.getInstance().width*0.8+100;
        var i = 0;
        for(tile in Ressource.tiles) {
            var bitmapTile = tile;
            var resIcon = new Bitmap(bitmapTile, ressourceBar);
            resIcon.setScale(0.4);
            resIcon.y = 15;
            resIcon.x = Window.getInstance().width*(i*0.08)+55;
            ressourceLabels[i] = new h2d.Text(DefaultFont.get(), ressourceBar);
            ressourceLabels[i].textColor = 0xfefefe;
            ressourceLabels[i].textAlign = Left;
            ressourceLabels[i].y = 15;
            ressourceLabels[i].x = Window.getInstance().width*(i*0.08) + 85;
            ressourceLabels[i].text = "" + AdvMap.currentTeam.resources[i];
            i++;
        }
        
        }
        heroUpdate();
        //console = new Console(font, parent);

        //parent.addChild(cmd);

        parent.addChild(nextTurnButton);
        parent.addChild(backButton);
        
    }

    public static function update(dt:Float) {

        fpsint = hxd.Timer.fps();
        fps.text = Std.string(Math.round(fpsint));
        
        
        lastMousePosition = currentMousePosition;
        currentMousePosition = new Point( Window.getInstance().mouseX, Window.getInstance().mouseY );

		#if debug
		if( K.isPressed("R".code) )
			speed = speed == 1 ? 0.1 : 1;	
		#end
       
		hxd.Timer.tmod *= speed;
		dt *= speed;

		key = {
			left : K.isDown(K.LEFT) || K.isDown("Q".code) || K.isDown("A".code) ,
			right : K.isDown(K.RIGHT) || K.isDown("D".code) ,
			up : K.isDown(K.UP) || K.isDown("Z".code) || K.isDown("W".code) ,
			down : K.isDown(K.DOWN) || K.isDown("S".code)  ,
			
		};
       
        
		/*if( key.right || (Window.getInstance().mouseX > Window.getInstance().width - 5)) {
			AdvMap.layers.x -= 10;
            AdvMap.fogLayer.x -= 10;
        }
        if( key.left || (Window.getInstance().mouseX < 5)) {
			AdvMap.layers.x += 10;
            AdvMap.fogLayer.x += 10;
        }
        if( key.up || (Window.getInstance().mouseY < 5)) {
			AdvMap.layers.y += 10;
            AdvMap.fogLayer.y += 10;
        }
        if( key.down || (Window.getInstance().mouseY > Window.getInstance().height - 5)) {
			AdvMap.layers.y -= 10;
            AdvMap.fogLayer.y -= 10;
        }*/
        if (K.isPressed(K.MOUSE_WHEEL_UP) && K.isDown(K.CTRL)) {
			Camera.zoomTo(AdvMap.layers, 1.25, currentMousePosition);
		}
        
		if (K.isPressed(K.MOUSE_WHEEL_DOWN) && K.isDown(K.CTRL)) {
			Camera.zoomTo(AdvMap.layers, 0.8, currentMousePosition);
        }

        if (K.isDown(K.MOUSE_LEFT) && K.isDown(K.CTRL)) {
            Camera.moveBy(AdvMap.layers, lastMousePosition.sub(currentMousePosition));
  
        }


        

        //disp.scrollDiscrete(1.2 * dt, 2.4 * dt);// for water shader
       
        
    AdvMap.layers.x = AdvMap.layers.x;
    }

    public static function onResize() {
        nextTurnButton.x = Window.getInstance().width - 75;
        nextTurnButton.y = Window.getInstance().height - 75;
        scrollBox.y = Window.getInstance().height - 144;
        {
        ressourceBar.removeChildren();
        ressourceBar.x = Window.getInstance().width * 0.1 - 50;
        ressourceBar.y = 3;
        var leftPart = new Bitmap(hxd.Res.MapUI.resBar.toTile(), ressourceBar);
        var coreTile = hxd.Res.MapUI.resBarCore.toTile();
        coreTile.scaleToSize(Window.getInstance().width*0.8, 50);
        var core = new Bitmap(coreTile, ressourceBar);
        core.x = 50;
        var rightPartTile = hxd.Res.MapUI.resBar.toTile();
        rightPartTile.flipX();
        var rightPart = new Bitmap(rightPartTile, ressourceBar);
        rightPart.x = Window.getInstance().width*0.8+100;
        var i = 0;
        for(tile in Ressource.tiles) {
            var bitmapTile = tile;
            var resIcon = new Bitmap(bitmapTile, ressourceBar);
            resIcon.setScale(0.4);
            resIcon.y = 15;
            resIcon.x = Window.getInstance().width*(i*0.08)+55;
            ressourceLabels[i] = new h2d.Text(DefaultFont.get(), ressourceBar);
            ressourceLabels[i].textColor = 0xfefefe;
            ressourceLabels[i].textAlign = Left;
            ressourceLabels[i].y = 15;
            ressourceLabels[i].x = Window.getInstance().width*(i*0.08) + 85;
            ressourceLabels[i].text = "" + AdvMap.currentTeam.resources[i];
            i++;
        }
        
        }
        


    }
    public static function resourceUpdate() {
        if (ressourceLabels != null) {
            var i = 0;
            for(tile in Ressource.tiles) {
                ressourceLabels[i].text = "" + AdvMap.currentTeam.resources[i];
                i++;
            }
        }       
    }
    public static function heroUpdate() {
        scrollBox = new ScrollBox(275,144);
        scrollBox.y = Window.getInstance().height - 144;
        parent.addChild(scrollBox);

        for (hero in AdvMap.currentTeam.heroList) {
            var heroText:String = Std.string(hero.movePoint + " / " + hero.maxMovePoint);
            var testHero =  new InfoBar(DefaultFont.get(), hero.heroName , heroText);
            var callBack = function(data:Any):Any {
                testHero.info.text = Std.string(data + " / " + hero.maxMovePoint);
                trace('it work : ' + data);
                return null;
            }
            var w = new Watcher("heroUI", callBack);
            hero.addObserver(w);
            scrollBox.add(testHero);
        }
    }

}