package scene.gameUI;

import haxe.DynamicAccess;
import h2d.col.Voronoi.Diagram;
import hxd.res.BitmapFont;
import hxd.res.DefaultFont;
import h2d.Tile;
import hxd.Res;
import h2d.Object;
import h2d.Font;
import h2d.Text;
import h2d.Bitmap;
import h2d.Interactive;
import Std;

/**
 * A button with centered text and bitmap background
 */

class TextButton extends Object {

   
    public var backgroundTile:Tile;
    public var background:Bitmap;
    /** used to assign event on the button like this buttonName.interactive.onClick = function {};**/
    public var interactive:Interactive;
    /** the centered text **/
    public var label:Text; 
    private var oldX:Float;
    private var oldY:Float;
    private var width:Float;
    private var height:Float;

    /**
     * Create a button with choosen width and height , and a centered text with choosen font, the background tile will deformed to fill the choosen dimension
     * you can assign event to the button by adding event to the interactive of the button like this buttonName.interactive.onClick = function {};
     * @param width 
     * @param height 
     * @param font 
     * @param text 
     * @param background 
     */

    public function new(width:Float, height:Float, font:Font, text:String = "", ?background:Tile) {
        super();
        
        this.width = width;
        this.height = height;
        
        if (background == null) {
            var random = Std.random(3);
            switch random {
                case 0: backgroundTile = Res.MainMenu.button0.toTile();
                case 1: backgroundTile = Res.MainMenu.button1.toTile();
                case 2: backgroundTile = Res.MainMenu.button2.toTile();
            }
            
            
        } else {
            backgroundTile = background;
        }
        
        
        label = new h2d.Text(font);
        label.textColor = 0xfefefe;
        label.maxWidth = width/6*5;
        label.textAlign = Center;
        label.x = (width - (width/6*5))/2;
        label.y = 0; // Change this if you change the font for good alignement

        label.text = text;
        label.smooth = true;
        backgroundTile.scaleToSize(width, height);
        this.background = new Bitmap(backgroundTile);
        this.background.blendMode = Alpha;
        this.background.smooth = true;
        this.addChild(this.background);
        this.addChild(label);
        interactive = new Interactive(width, height, this);
        interactive.onOver = function( e : hxd.Event ) {
            this.oldX = this.x;
            this.oldY = this.y;
            this.x = this.x + this.width*0.05;
            this.y = this.y + this.height*0.05;
            this.setScale(0.9);
            
            
        }
        interactive.onOut = function( e : hxd.Event ) {
            this.setScale(1);
            this.x = oldX;
            this.y = oldY;
            
        }

    }

}
