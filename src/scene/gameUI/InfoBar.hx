package scene.gameUI;


import h2d.Bitmap;
import hxd.Window;
import h2d.Flow;
import h2d.Text;
import h2d.Font;
import h2d.Tile;

class InfoBar extends Flow { 

    public var nameTag:Text;
    public var info:Text;
    public var logo:Bitmap;
    

    /**
     * create text bar for the map selection menu
     * @param font used font
     * @param text text displayed
     * @param background background tile displayed
     */

    public function new(font:Font, nameText:String, infoText:String, ?tile:Tile, ?background:Tile) {
        super();
        
        if (background == null) {
            
            this.backgroundTile = h2d.Tile.fromColor(0x1d1d1d);
            
        } else {
            this.backgroundTile = background;
        }
        
        padding = 7;
        borderWidth=0;
        maxWidth = 270;
        overflow = true;
        layout = Stack;
        maxHeight = 46;

        logo = new Bitmap(Tile.fromColor(Std.random(16777215), 32, 32));

        nameTag = new h2d.Text(font);
        nameTag.textColor = 0xfefefe;
        nameTag.textAlign = Center;
        nameTag.text = nameText;
        nameTag.x = 248;

        nameTag.smooth = true;

        info = new h2d.Text(font);
        info.textColor = 0xfefefe;
        info.textAlign = Right;
        info.text = infoText;
        info.x = 248;
        info.smooth = true;

        this.background.blendMode = Alpha;
        this.background.smooth = true;
        addChild(logo);
        addChild(nameTag);
        addChild(info);
        enableInteractive = true;
		interactive.cursor = Button;
        interactive.onOver = function( e : hxd.Event ) {
             this.backgroundTile = h2d.Tile.fromColor(0x2d2d2d);
        }
        interactive.onOut = function( e : hxd.Event ) {
            this.backgroundTile = h2d.Tile.fromColor(0x1d1d1d);
            
        }
        

    }

    

    
}