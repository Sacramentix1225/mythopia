package scene.gameUI;

import haxe.macro.Expr.Catch;
import h2d.RenderContext;
import h2d.Interactive;
import h2d.Slider;
import h2d.Flow;
import h2d.Object;
import h2d.Tile;
import hxd.res.DefaultFont;

class ScrollBox extends ScrollArea {
    
    public var barBuffer:Array<Flow>;
    public var index:Int = 0;
    public var flowBox:Flow;
    public var slider:Slider;

    public function new(w:Int, h:Int) {
        super(w, h);
        barBuffer = new Array<Flow>();
        flowBox = new h2d.Flow(this);
		flowBox.layout = Vertical;
		flowBox.verticalSpacing = 1;
		flowBox.padding = 2;
        flowBox.minWidth = w;
        flowBox.minHeight = h;
        flowBox.maxHeight = h;
        flowBox.fillHeight = true;
        flowBox.backgroundTile = Tile.fromColor(0x5d5d5d);
        flowBox.backgroundTile.scaleToSize(flowBox.outerWidth, flowBox.outerHeight);
        flowBox.enableInteractive = true;
        flowBox.interactive.cursor = Button;
        flowBox.interactive.onWheel = function( e : hxd.Event ) {


        }

        

    }
    public function add(child:Flow) {
        child.enableInteractive = true;
        child.interactive.cursor = Button;

        child.interactive.onWheel = function( e : hxd.Event ) {
            if ( e.wheelDelta > 0 ) {
                index++;
                if (index > barBuffer.length-1) index = 0;
            }
            else if (e.wheelDelta < 0) {
                index--;
                if (index < 0) index = barBuffer.length-1;
            }
            flowBox.removeChildren();
            for (i in index...(index+3)) {
                var j:Int;
                if (i > barBuffer.length-1 ) j = i-barBuffer.length;
                else j = i;
                try {
                    flowBox.addChild(barBuffer[j]);
                } catch(err:Dynamic) {
                    trace(err);
                }
            }
            
        }
        barBuffer.push(child);
        flowBox.removeChildren();
        for (i in index...(index+3)) {
            var j:Int;
            if (i > barBuffer.length-1 ) j = i-barBuffer.length-1;
            else j = i;
            try {
                flowBox.addChild(barBuffer[j]);
            } catch(err:Dynamic) {
                trace(err);
            }
        }

    }

}