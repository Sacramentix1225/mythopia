package scene;


import h3d.pass.ColorMatrix;
import h2d.filter.Blur;
import h2d.Bitmap;
import h2d.Object;
import h2d.Scene;
import hxd.Window;
import h2d.Tile;
import h2d.Text;
import h2d.Font;
import h2d.Layers;
//import h2d.filter.Ambient;
//import h2d.filter.Bloom;
import hxd.Key in K;
//import h2d.filter.Filter;
import map.TileCollider;
import map.object.Hero;
import map.object.Ressource;
import map.object.Team;
import map.TileProperty;
import setting.Tileset;
import hxd.res.TiledMap;
import h2d.col.Point;
import scene.gameUI.TextButton;

 
class AdvMap extends DynamicScene
{
    
    // Global variable of the game can be accesed  with Game.(insertVar) in any class

    /**Boolean map of the current map true if the position is walkable or false if the position is obstructed&&&  **/
    public static var collisionMap:Array<Array<Bool>>;
    /** Contains all layer of the map**/
    public static var layers:h2d.Layers;
    public static var map:Array<Array<Array<Int>>>;
    public static var currentSelectedTile:Point;
    public static var lastSelectedTile:Point; 
    /**list that contains every sprite of the game**/
    public static var tileImage:Tile;
    public static var tilesList:Array<Tile>;
    /** current map width **/
    public static var fogLayer:Object;
    public static var teamList:Array<Team>;
    public static var currentTeam:Team;

    public static var mw:Int;
    public static var mh:Int;
        
    
    
    /**
     * generate a new adventure map with the tmx file tranformed to map
     * @param tiledMapData  a tmx file transformed with the function toMap() of hxd.res.TiledMap;
     */


    public function new(tiledMapData:TiledMapData) // argument array for team info
    {   
        super();
        // load map data
        var mapData:TiledMapData = tiledMapData;
        // get tile image (tiles.png) from resources
        
        // map size
        mw = mapData.width;
        mh = mapData.height;
        // A loop to parse tile with the correct dimension
        tilesList = new Array<h2d.Tile>();
        
        Tileset.init();
        for (y in 0...4) //ground and deco tile
        {
            for (x in 0...4)
            {
                var t = Tileset.tile.sub(x * Tileset.w, y * Tileset.h, Tileset.w, Tileset.h);
                tilesList.push(t);
            }
            
        }
        Ressource.init();
        currentSelectedTile = new Point(-1, -1);
        lastSelectedTile = new Point(-1, -1); 
        // create h2d.Layers object and add it to 2d scene
        layers = new h2d.Layers(this);
        var ressourceLayers = new Object();
        ressourceLayers.name = "res";
        layers.add(ressourceLayers, 3);
        map = new Array<Array<Array<Int>>>();
        map = [for (y in 0...mh) [for (x in 0...mw) [for (n in 0...3) 0]]];
        
        // iterate over all layers
        for (layer in mapData.layers)
        {
            // get layer index
            var layerIndex:Int = mapData.layers.indexOf(layer);
            // create tile group for this layer
            var layerGroup:h2d.TileGroup = new h2d.TileGroup(tilesList[0]);
            switch layerIndex {
                case 0 : layerGroup.name = "ground";
                        layers.add(layerGroup, layerIndex);
                case 1 : layerGroup.name = "deco";    
                    layers.add(layerGroup, layerIndex);
                
            }
            // and add it to layers object at specified index
 
            
            // you can also add objects to layer over and under specified objects
            // by using `over()` and `under()` methods
 
            // iterate on x and y
            for (y in 0...mh)
            {
                for (x in 0...mw)
                {
                    // get the tile id at the current position
                    var tid = layer.data[x + y * mw];
                    map[x][y][layerIndex] = tid;
                    if (tid != 0) // skip transparent tiles
                    {
                        if (tid > 12) {
                            var currentTile = new map.object.Ressource(tid-13, x, y);
                            // add a tile to layer
                            
                            ressourceLayers.addChild(currentTile);
                            continue;
                        }
                        // create a new AdvTile with a hitbox for click 
                        if (layerIndex==0) { 
                            layerGroup.add(-0.5*Tileset.tw +(x - y) * Tileset.tw/2, -(Tileset.th+Tileset.th*mh*0.5)+(x + y) * Tileset.th/2 + 64, tilesList[tid-1]);
                        } else {
                            layerGroup.add(-0.5*Tileset.tw +(x - y) * Tileset.tw/2, -(Tileset.th+Tileset.th*mh*0.5)+(x + y) * Tileset.th/2, tilesList[tid-1]);
                        }
                        
                    }
                }
            }
            
            // you can also iterate through all objects on specified layer with `getLayer()` method
        }
        var interactiveLayers = new Object(layers);
        for (y in 0...mh) {
            for (x in 0...mw) {
                interactiveLayers.addChild(new TileCollider(x, y));
            }  
        }
        fogLayer = new Object();
        for (y in 0...mh) {
            for (x in 0...mw) {
                var fog = new Object();
                fog.x = -0.5*Tileset.tw+(x - y) * Tileset.tw/2;
                fog.y = -(Tileset.th+Tileset.th*mh*0.5)+(x + y) * Tileset.th/2;
                var bitmap = new Bitmap(tilesList[12], fog);
                fogLayer.addChild(fog);     
            }  
        }
        layers.add(fogLayer, 4);
        layers.x = Window.getInstance().width *0.5;
        layers.y = Window.getInstance().height *0.5;
        var redPoint = Tile.fromColor(0xff0000,15,15);
        layers.addChild(new Bitmap(redPoint));


        teamList = new Array<Team>();
        var team0 = new Team(0, 0xff0000, "red dragon", "sample text 1");
        currentTeam = team0;
        var team1 = new Team(1, 0x0000ff, "blue sheep", "sample text 1");
        teamList.push(team0);
        teamList.push(team1);
        var hero0 = new Hero(team0, "staline", "sample text 0", 0, 0, tilesList[13], layers.getObjectByName("deco"));
        var hero1 = new Hero(team1, "jake", "sample text 1", 1, 1, tilesList[13], layers.getObjectByName("deco") );
        var hero2 = new Hero(team0, "enilats", "sample text 0", 2, 2, tilesList[13], layers.getObjectByName("deco"));
        var hero3 = new Hero(team1, "paul", "sample text 1", 3, 3, tilesList[13], layers.getObjectByName("deco") );
        var hero4 = new Hero(team0, "etalins", "sample text 0", 4, 4, tilesList[13], layers.getObjectByName("deco"));
        for (hero in team0.heroList) {
            hero.adjustColor({saturation : 0.5, lightness : 0.5, hue : 170});
        }

        

        fogUpdate();
        AdvMapUI.init(this);
    
    }

    /**
    * code below is executed each time before a frame is generated
    * @param dt time beetwen the last frame and the current frame
    */

    override public function update(dt:Float) {
        AdvMapUI.update(dt);


    }

    /**
     * code below is executed each time the window is resized ( don't work on js )
     */

    override public function onResize() {
        
        AdvMapUI.onResize();
    }
    public static function fogUpdate() {
        var px = 0;
        var py = 0;
        //var invisible = new h2d.filter.Glow(0xffffff,0,0);
        //invisible.knockout = true;
        //invisible.smooth = false;
        var m = new h3d.Matrix();
      m.identity();
      m.colorGain(0xffffff, 0);
      var invisi = new h2d.filter.ColorMatrix(m);

        for (object in fogLayer) {
            if( currentTeam.fogOfWar[py][px] == false) object.visible = false; 
            else object.visible = true;
            if (px == mw-1) {
                py++;
                px=0;
            } else {
                px++;
            }
            //trace(py + " " + px);
        }
        
    }       
            

}