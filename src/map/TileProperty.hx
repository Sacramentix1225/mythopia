package map;

import h2d.Font;

/**
 * this class contain various of static that are used for tile
 */

class TileProperty {

    public static var BlockedTileList = [7,8,9,10,11];

    public static function isTileBlocked(tid:Int) {
        if (BlockedTileList.indexOf(tid) != -1 ) {
            return true;
        } else {
            return false;
        }
    }
    

}