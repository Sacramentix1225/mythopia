package map;

import pattern.Observer;
import scene.AdvMapUI;
import map.object.Ressource;
import scene.AdvMap;

class Turn {
    public static function init() {

    }

    public static function next() {
        if (scene.AdvMap.currentTeam.id + 1 >  scene.AdvMap.teamList.length-1) scene.AdvMap.currentTeam = scene.AdvMap.teamList[0];
                else scene.AdvMap.currentTeam = scene.AdvMap.teamList[scene.AdvMap.currentTeam.id+1];
                scene.AdvMap.currentTeam.resetMovepoint();
                try {
		            scene.AdvMapUI.moveInfo.text = scene.AdvMap.currentTeam.heroList[0].movePoint + " / " + scene.AdvMap.currentTeam.heroList[0].maxMovePoint;
	            } catch (err:Dynamic) {
		
	            }
                
                AdvMap.currentTeam.resources[Ressource.gold] += 1000;
                AdvMapUI.resourceUpdate();
                AdvMapUI.heroUpdate();
                trace(Observables.observables);
                scene.AdvMap.fogUpdate();

    }
}