package map;

import h2d.col.Point;
import hxd.Window;
import h2d.Object;

class Camera   {

    public var zoomLevel:Float;


    public static function zoomTo(map:Object, factor:Float, point:Point):Void {
        var mapPoint = map.globalToLocal(point);
        map.x += mapPoint.x*map.scaleX;
        map.y += mapPoint.y*map.scaleY;
		map.scale(factor);
        map.x -= mapPoint.x*map.scaleX;
        map.y -= mapPoint.y*map.scaleY;
        
    }

    public static function moveBy(map:Object, vector:Point):Void {
        map.x -= vector.x;
        map.y -= vector.y;
		
        
    }

}