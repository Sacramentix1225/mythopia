package map.object;
import h2d.Bitmap;
import setting.Tileset;

import h2d.Tile;
import h2d.Object;
import scene.AdvMap;

class Ressource extends Bitmap {
    public static var gold:Int = 0;
    public static var wood:Int = 1;
    public static var stone:Int = 2;
    public static var iron:Int = 3;
    public static var silk:Int = 4;
    public static var faith:Int = 5;
    public static var knowledge:Int = 6;
    public static var gems:Int = 7;
    public static var techpoint:Int = 8;
    public static var crate:Int = 9;
    public static var tiles:Array<Tile>;

    public static function init() {
        tiles = new Array<Tile>();
        for (y in 0...2) //ground and deco tile
        {
            for (x in 0...8)
            {
                if (y>=1 && x>=2) break;
                var t = Tileset.tile.sub(x * Tileset.w * 0.5, 4*Tileset.h + (y * Tileset.h * 0.5), Tileset.w * 0.5, Tileset.h * 0.5);
                trace(4*Tileset.h + (y * Tileset.h * 0.5) + " " + x * Tileset.w * 0.5 +" ");
                trace(t);
                tiles.push(t);
            }
            
        }
    }

    /** x position on the tilemap in tile**/
    public var tx:Int;
    /** y position on the tilemap in tile**/
    public var ty:Int;
    /** x position on the layer **/


    public function new(ressourceType:Int, tx:Int, ty:Int) {
        super();
        this.x = -0.5*Tileset.th +(tx - ty) * Tileset.tw/2 + Tileset.tw/4;
        this.y = -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + Tileset.th;
        this.tile = tiles[ressourceType];
        this.tx = tx;
        this.ty = ty;
        
    }
}