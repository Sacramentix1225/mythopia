package map.object;




import scene.AdvMap;
import scene.AdvMapUI;
// team 0 will be the deafult team where all npc are
class Team {
    
    public var id:Int;
    public var color:Int; // hexadecimal format int like FF0000 for red
    public var name:String;
    public var description:String;
    public var heroList:Array<Hero>;
    //public var castleList:Array<Castle>;
    public var resources:Array<Int>; // Can be accesed with ressource name like hxd.key does
    public var resourcesIncome:Array<Int>;
    public var fogOfWar:Array<Array<Bool>>; // boolean map  to check if a tile is covered by a fog tile
   
    public function new(id:Int, color:Int, name:String, description:String) {
        this.id = id;
        this.color = color;
        this.name = name;
        this.description = description;
        heroList = new Array<Hero>();
        resources = new Array<Int>();
        resources = [10000, 50, 50, 25, 0, 0, 0, 0, 0];
        trace(resources[9]);
        resourcesIncome = new Array<Int>();
        fogOfWar = new Array<Array<Bool>>();
        fogOfWar = [for (y in 0...AdvMap.mh) [for (x in 0...AdvMap.mw) true]];
    }
    public function iterator() {
    return heroList.iterator();
    }

    public function revealFog(tx, ty, range) {
        for( y in ty-range...ty+range+1) {
            if(y<0) continue;
            if(y>AdvMap.mh) continue;
            for( x in tx-range...tx+range+1) {
                if(x<0) continue;
                if(x>AdvMap.mw) continue;
                try {
                fogOfWar[y][x] = false;
                AdvMap.fogLayer.getChildAt(y*AdvMap.mw+x).visible = false;
                } catch(err:Dynamic) {}
               
            }
        }   
    }
    public function resetMovepoint() {
        for ( hero in this ) {
            hero.movePoint = hero.maxMovePoint;
        }
    }
    
}
