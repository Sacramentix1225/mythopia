package map;

import h2d.filter.Filter;
import h2d.col.Polygon;
import h2d.col.Point;
import h2d.col.PolygonCollider;
import h2d.Interactive;
import h2d.Tile;
import scene.AdvMap;
import scene.AdvMapUI;
import setting.Tileset;
import h2d.Graphics;

/**
 * called by Advmap to create each tile and create hitbox over it
 */

class TileCollider extends Interactive {

public var graphics:Graphics;

public function new(tx:Int, ty:Int) {
    super( Tileset.tw, Tileset.th);

    var tileShape = new Polygon([
			new Point(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 +   Tileset.th * 0.5), 
			new Point(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2 +   Tileset.tw * 0.5,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 ), 
			new Point(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2 +   Tileset.tw,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 +   Tileset.th * 0.75), 
			new Point(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2 +   Tileset.tw * 0.5,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 +   Tileset.th), 
			
		]);
    graphics = new Graphics();
    graphics.beginFill(0xff0000, 0.2);
    graphics.lineTo(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 +   Tileset.th * 0.5);
    graphics.lineTo(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2 +   Tileset.tw * 0.5,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 );
    graphics.lineTo(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2 +   Tileset.tw,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 +   Tileset.th * 0.5);
    graphics.lineTo(   -0.5*Tileset.tw+(tx - ty) * Tileset.tw/2 +   Tileset.tw * 0.5,     -(Tileset.th+Tileset.th*AdvMap.mh*0.5)+(tx + ty) * Tileset.th/2 + 64 +   Tileset.th);
    graphics.endFill();
    //this.addChild(graphics);

    this.shape = tileShape.getCollider();
    this.onClick = function( e : hxd.Event ) {
	    trace(this);
        AdvMap.lastSelectedTile = AdvMap.currentSelectedTile;
        AdvMap.currentSelectedTile = new Point(tx, ty);
        AdvMapUI.tileInfo.text = "current tile : " + AdvMap.currentSelectedTile.toString() + ", last tile : " + AdvMap.lastSelectedTile.toString() + " " + AdvMap.currentTeam.heroList[0];

        for (hero in AdvMap.currentTeam) {
            if (AdvMap.lastSelectedTile == null) break;
            if ( (Std.int(AdvMap.lastSelectedTile.x) == hero.getTx() ) && (Std.int(AdvMap.lastSelectedTile.y) == hero.getTy()) ) {
                hero.calcPath();
            
            }
        }
    }
    
    
    }
    
}
 
 