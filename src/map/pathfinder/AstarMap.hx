package map.pathfinder;

import pathfinder.IMap;
import scene.AdvMap;

/**
 * this class is used by the pathfinder algorythme to check if a tile is walkable
 */

class AstarMap implements IMap
{	
	public var rows( default, null ):Int;
	public var cols( default, null ):Int;

	/**
	 * create a dimension of map for pathfinder algorythme
	 * @param p_cols number of colums of the map
	 * @param p_rows number of rows of the map
	 */

	public function new( p_cols:Int, p_rows:Int )
	{
		cols = p_cols;
		rows = p_rows;
		
	}
	
	/**
	 * check if a tile of the map is walkable
	 * @param p_x x position in tile on the map
	 * @param p_y y position in tile on the map
	 * @return Bool , true if the tile is walkable and false if the tile is obstructed
	 */

	public function isWalkable( p_x:Int, p_y:Int ):Bool{
		var blocked:Array<Int> = [0,7,8];
        var result:Bool;
		if (blocked.indexOf(AdvMap.map[p_x][p_y][0]) == -1) result = true;
		else result = false;
		return result;

	}
}

